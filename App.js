import { createAppContainer } from 'react-navigation'
import AddView from './components/AddView';
import { Dimensions, StyleSheet, Text, SafeAreaView } from 'react-native';
import MainView from './MainView';
import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import SearchView from './components/SearchView';
import LoadingScreen from './components/LoadingScreen';
import * as Firebase from 'firebase';
import 'firebase/firestore';
// color scheme
// #C5C1C0
// #0A1612
// #1A2930
// #F7CE3E

// initialize firebase

const firebaseConfig = {
    apiKey: "AIzaSyDYZG-aXONiFL-_o5MW_OxhHlpimywpI3s",
    authDomain: "bar-spec.firebaseapp.com",
    databaseURL: "https://bar-spec.firebaseio.com",
    projectId: "bar-spec",
    storageBucket: "bar-spec.appspot.com",
}
Firebase.initializeApp(firebaseConfig);
const db = Firebase.firestore();


var Navigator;
const dim = Dimensions.get('window');
const styles = StyleSheet.create({
  nav: {
    height: dim.height,
    width: dim.width,
    backgroundColor: "#e3d3f3"
  }
})

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
    this.getCurrentLocation = this.getCurrentLocation.bind(this);
  }
  componentDidMount = () => {
    const RootStack = createStackNavigator({
      Search: {
        screen: (props) => <SearchView {...props} location={this.state.location} />,

      },
      Home: {
        screen: (props) => <MainView database={db} {...props} location={this.state.location} />,
        header: 'home'
      },
      Add: {
        screen: AddView
      }
    }, {
      initialRouteName: 'Search',
      headerMode: 'none'
    });


    Navigator = createAppContainer(RootStack);
    this.getCurrentLocation();
  }

  getCurrentLocation = () => {
    navigator.geolocation.getCurrentPosition(
      async (position) => {
        let lat = parseFloat(position.coords.latitude).toFixed(4);
        let lng = parseFloat(position.coords.longitude).toFixed(4);
        let content = { lat: lat, lng: lng };
        console.log("hitting the server");
        fetch("https://us-central1-bar-spec.cloudfunctions.net/reverseGeocode", {
          method: "POST",
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(content)
        }).then(res => res.json()).then(
          data => {
            let currentLoc = {
              city: data.city,
              state: data.state_abbr
            }
            console.log(JSON.stringify(currentLoc));
            this.setState({ location: currentLoc });
          });

      },
      // error => Alert.alert(error.message),
      // { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }
  render() {
    return (
      this.state.location ?
        <Navigator style={styles.nav} /> :
        <LoadingScreen />
    )
  }
}

