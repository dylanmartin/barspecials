const functions = require('firebase-functions');

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions
const reverse = require('reverse-geocode')
const express = require('express');
const cors = require('cors');
const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

app.post('/', 
    async (request, response) => {
        let data = request.body;
        let result = await reverse.lookup(data.lat, data.lng, 'us');
        response.json(result);
});


// Expose Express API as a single Cloud Function:
exports.reverseGeocode = functions.https.onRequest(app);
