import React from 'react';
import {Text,View} from 'react-native';
export default class SpecialCell extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.name,
            special: props.special
        }
    }

    render() {
        return (
            <View>
                <Text>
                    Name:{this.state.name}
                </Text>
                <Text>
                    Special:{this.state.special}
                </Text>
            </View>
        )
    }
}