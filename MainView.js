import React from 'react';
import SpecialsView from './components/SpecialsView';
import Navbar from './components/Navbar';
import { AdMobBanner } from 'expo-ads-admob'
import { SafeAreaView, StyleSheet, Dimensions } from "react-native";
import LoadingScreen from './components/LoadingScreen';
const { height, width } = Dimensions.get('window');

export default class MainView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            specials: [],
            isloading: true
        }
        this.getDataFromDB = this.getDataFromDB.bind(this);
    }

    getDataFromDB = () => {
        return new Promise((res, rej) => {
            let db = this.props.database;
            let tmp = {};
            let restaurantsRef = db.collection("States").doc(this.props.location.state).collection("City").doc(this.props.location.city).collection("Restaurants");
            restaurantsRef.get().then(snapshot => {
                snapshot.forEach(doc => {
                    tmp[doc.id] = doc.data();
                });
            }).then(() => {
                console.log(JSON.stringify(tmp,null,2));
                this.setState({
                    specials: tmp,
                    isloading: false
                });
                res(tmp);
            }).catch(err => {
                console.log("Error when trying to fetch documents", err);
                rej()
            });
        });
    }

    componentDidMount() {
        this.getDataFromDB();
    }

    render() {
        return (
            this.state.isloading ?
                <LoadingScreen />
                : <SafeAreaView style={styles.container}>
                    {/* <Navbar navigation={this.props.navigation} /> */}
                    <SpecialsView getData={this.getDataFromDB} location={this.props.location} specials={this.state.specials} />
                    <AdMobBanner
                        bannerSize="fullBanner"
                        // adUnitID="ca-app-pub-9637168015703039/5442023395"
                        adUnitID="ca-app-pub-3940256099942544/6300978111" // Test ID, Replace with your-admob-unit-id
                        testDeviceID="EMULATOR"
                        style={styles.adUnit}
                    />
                </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1a2930',
        alignItems: 'center',
        justifyContent: 'center',
    },
    addUnit: {
        width: width
    }
});
