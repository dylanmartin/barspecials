import React from 'react';
import { Text, View, StyleSheet, Dimensions } from 'react-native';
var {height, width} = Dimensions.get('window');

export default class SpecialCell extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.name,
            special: props.special
        }
    }
    render() {
        return (
        <View id="container" style={styles.container}>
            <Text style={styles.name}>
                <Text style={styles.label}>Name: </Text>{this.state.name}
            </Text>
            <Text style={styles.text}>
                <Text style={styles.label}>Special: </Text>{this.state.special}
            </Text>
        </View>
        )
    }
}
var styles = StyleSheet.create({
    label: {
        color: '#373737',
        fontFamily: 'Arial',
        fontSize: 20,
        fontWeight: 'bold',
    },
    name: {
        color: '#373737',
        fontFamily: 'Arial',
        fontSize: 20,
    },
    text: {
        color: '#373737',
        fontFamily: 'Arial',
        fontSize: 15,
    },
    container: {
        justifyContent: 'space-between',
        alignItems: 'baseline',
        padding: 30,
        right:width,
        left:0,
        margin:0,
        // borderRadius: 10,
        backgroundColor: '#f4f4f4'
    }
});

