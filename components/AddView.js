import React from 'react';
import { Text, StyleSheet, TextInput, View, Button } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import Navbar from './Navbar'
export default class AddView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
        this.addRestaurant = this.addRestaurant.bind(this);
    }
    addRestaurant = () => {
        let db = admin.firestore();
        let restaurantsRef = db.collection("States").doc("DE").collection("City").doc("Newark").collection("Restaurants");
        for (let obj of tmpdb) {
            let name = obj.Name;
            restaurantsRef.add(obj).then(ref => {
                console.log("Added document with ID:", ref.id);
            }).catch(err => {
                console.log("Error uploading file => ", err)
            });
        }
    }

   render() {
        return (
            <SafeAreaView style={styles.container}>
                <Navbar navigation={this.props.navigation}/>
                <View style={styles.view}>
                    <Text style={styles.text}>
                        Name: 
                    </Text>
                    <TextInput placeholderTextColor="#0A1612" placeholder='Restaurant Name' style={styles.input}>
                        
                    </TextInput>
                </View> 
                <View style={styles.view}>
                    <Text style={styles.text}>
                        Name: 
                    </Text>
                    <TextInput placeholderTextColor="#0A1612" placeholder='Restaurant Name' style={styles.input}>
                        
                    </TextInput>
                </View> 
                <Button color="#0A1612" title="Submit" style={styles.button} />
            </SafeAreaView>
        );
    }
}


const styles = StyleSheet.create({
    container:{
        backgroundColor:"#1A2930",
        flex:1
    },
    text: {
        color: "#F7CE3E",
        fontFamily: 'Arial',
        fontWeight:'bold',
        marginTop:'1%',
        marginBottom:'1%'
    },
    input: {
        backgroundColor:'#C5C1C0',
        color: '#0A1612',
        borderColor: '#888888',
        borderWidth: 1,
        borderRadius: 10,
        fontFamily: 'Arial',
        textAlignVertical:'center',
        marginTop:'1%',
        marginBottom:'1%'
    },
    view: {
        flexDirection:"column",
        width:'90%',
        height: '15%',
        margin: '5%' 
    },
    button: {
        backgroundColor:'#F7CE3E',
        color:'#0A1612',
        borderColor: '#888888',
        borderWidth: 1,
        borderRadius: 10,
    }
})