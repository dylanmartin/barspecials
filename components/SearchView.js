import React from 'react';
import { SafeAreaView, Dimensions, Text, View, Button, StyleSheet, Slider } from 'react-native';
// import Slider from '@react-native-community/slider';

export default class SearchView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            distance: 5,
        };
        this.handleSliderValueChange = this.handleSliderValueChange.bind(this);
    }

    
    handleSliderValueChange = (event) => {
        this.setState({ distance: Math.floor(event) })
    }

    render() {
        return (
            <SafeAreaView style={styles.view}>
                <View style={styles.container}>
                    <Text>
                        Your Current Location is: {this.props.location.city}, {this.props.location.state}
                    </Text>
                    <Text style={styles.text}>
                        Your selected Distance is: {this.state.distance} miles
                    </Text>
                    {/* Slider is depricated but other one is not updated for new ios yet */}
                    <Slider
                        style={{ width: 200, height: 40 }}
                        minimumValue={0}
                        maximumValue={25}
                        value={5}
                        onValueChange={(event) => this.handleSliderValueChange(event)}
                    />
                    <View style={styles.button} >
                        <Text styles={styles.buttonText} onPress={() => this.props.navigation.navigate("Home")}>
                            Search
                        </Text>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const dimensions = Dimensions.get('window');

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#1a2930',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        padding: dimensions.width / 5,
        margin: "auto",
        justifyContent: "center",
        backgroundColor: '#eeeeee',
        borderRadius: dimensions.width / 20
    },
    text: {
        color: '#0a2930',
        fontFamily: 'Arial',
        fontSize: 20,
        fontWeight: 'bold',
    },
    button: {
        backgroundColor: "#F7CE3E",
        borderRadius: dimensions.width / 50,
        padding: dimensions.width / 20,
        color: '#1a2930',
        justifyContent: "center",
        textAlign: "center"
    },
    buttonText: {
        height: "100%",
        width: "100%",
        color: '#0a2930',
        fontFamily: 'Arial',
        fontSize: 20,
        fontWeight: 'bold',

    }
});