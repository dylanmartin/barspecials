import React from "react";
import { View, Text, StyleSheet, Dimensions } from 'react-native';
export default class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Text allowFontScaling style={styles.text} onPress={() => this.props.navigation.navigate("Home")}>
                    Home
                </Text>
                <Text allowFontScaling style={styles.text} onPress={() => this.props.navigation.navigate("Add")}>
                    Add
                </Text>
            </View>
        );
    }
}

const dimensions = Dimensions.get('window');

const styles = StyleSheet.create({
    text:{
        color:'#f7ce3e',
        fontFamily:'Arial',
        textAlign: 'center',
        textAlignVertical: 'center',
        flex:1,
        fontSize:dimensions.height/40,
        margin:dimensions.height/80,
    },
    container: {
        height:dimensions.height/20,
        width: dimensions.width,
        flexDirection:"row",
        backgroundColor:'#1a2930'
    }
})