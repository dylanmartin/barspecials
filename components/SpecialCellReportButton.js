import React from 'react';
import { Text, View, StyleSheet, Dimensions, Modal, TouchableHighlight } from 'react-native';
import * as Firebase from 'firebase';
import 'firebase/firestore';

const { height, width } = Dimensions.get('window');
export default class ReportButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false
        };
        this.handleReport = this.handleReport.bind(this);
        this.setModalVisible = this.setModalVisible.bind(this);
    }

    setModalVisible = (visible) => {
        this.setState({modalVisible:visible})
    }

    handleReport = () => {
        console.log("Report button clicked!");
        let db = Firebase.firestore();
        let restaurantsRef = db.collection("States").doc(this.props.location.state).collection("City").doc(this.props.location.city).collection("Restaurants").doc(this.props.id);
        restaurantsRef.get().then(doc => {
            if (!doc.exists) {
                console.log('Aww shit the document is gone! Someone stole it!');
            } else {
                // updata data
                let res = doc.data().Reports;
                let today = (new Date().getDay() + 6) % 7;
                // res[today] = "";
                // restaurantsRef.update({Specials: res}).catch(err => {
                //     console.log("Aww fuck there was an error when updating special:", err);
                // });
                // If there is currently not a report array, make a new one and add it.
                if (!res) {
                    let reportArr = new Array(6).fill(0);
                    reportArr[today]++;
                    restaurantsRef.update({Reports:reportArr}).then(ref => {
                        console.log("Added report array");
                    }).catch(err => {
                        console.log("Error uploading new report array => ", err)
                    });
                }
                // Otherwise update the existing one.
                else {
                    res[today]++;
                    restaurantsRef.update({Reports:res}).then(ref => {
                        console.log("Updated report array");
                    }).catch(err => {
                        console.log("Error updating existing report array => ", err)
                    });
                }
            }
            this.props.delete(this.props.id);
        }).catch(err => {
            console.log("Aww fuck theres an error reading in the data: ", err);
        });

    }

    render() {
        return (
            <View onTouchEnd={() => this.setModalVisible(!this.state.modalVisible)} style={styles.container}>
                <Text style={styles.text}>Report</Text>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <View style={styles.modal}>
                        <View style={styles.modalView}>
                            <Text>Are you sure you want to report {this.props.special} at {this.props.name}</Text>

                            <TouchableHighlight
                                style={styles.button}
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                    this.handleReport();
                                }}>
                                <Text style={styles.text}>Yes I'm Sure!</Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                style={styles.button}
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                }}>
                                <Text style={styles.text}>No I am not!</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

let styles = StyleSheet.create({
    text: {
        fontFamily: 'Arial',
        textAlign: 'center',
        alignSelf: 'center',
        color:"#eeeeee"
    },
    container: {
        width: width,
        height: '100%',
        marginLeft: width - Math.ceil(width / 4),
        backgroundColor: 'red',
        width: Math.ceil(width / 4),
        padding: 0,
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center'
    },
    modal:{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:'rgba(204, 204, 204, 0.7)',
        position:'absolute',
        top:0,
        bottom:0,
        left:0,
        right:0
    },
    modalView: {
        padding: width / 5,
        margin: "auto",
        justifyContent: "center",
        backgroundColor: '#eeeeee',
        borderRadius: width / 20
    },
    button: {
        marginTop: width/20,
        backgroundColor: "#147efb",
        borderRadius: width / 50,
        padding: width / 20,
        justifyContent: "center",
        textAlign: "center"
    }
});

