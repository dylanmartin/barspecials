import React from 'react';
import { ScrollView, Dimensions, StyleSheet, RefreshControl, Modal } from 'react-native';
import SpecialCell from './SpecialCell';
import ReportButton from './SpecialCellReportButton';
import { SwipeRow } from 'react-native-swipe-list-view';
const { height, width } = Dimensions.get('window');
export default class SpecialsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            specials: props.specials,
            refreshing: false,
            modalVisible: false
        };
        this.renderSpecials = this.renderSpecials.bind(this);
        this.deleteSpecial = this.deleteSpecial.bind(this);
        this.onRefresh = this.onRefresh.bind(this);
    }

    // Removes a special from this.state.special
    deleteSpecial = (key) => {
        let tmp = this.state.specials;
        delete tmp[key];
        this.setState({ specials: tmp });
    }

    onRefresh = () => {
        console.log("Refreshing....");
        this.setState({ refreshing: true });
        this.props.getData().then((res) => {
            this.setState({ specials: res, refreshing: false });
            console.log("Done")
        })
    }

    // Renders all the specials stored in this.state.specials
    renderSpecials = () => {
        let specials = new Array();
        if (this.state.specials) {
            if (this.state.specials.length !== 0) {
                let today = (new Date().getDay() + 6) % 7;
                for (let key of Object.keys(this.state.specials)) {
                    let obj = this.props.specials[key];
                    // skip restaurant if there is no event for today
                    if (!obj.Specials[today] || obj.Specials[today].toString() === "") {
                        continue;
                    }
                    console.log(obj.Specials[today]);
                    specials.push(
                        <SwipeRow tension={100} friction={100} style={styles.swipeRow} rightOpenValue={-1 * Math.floor(width / 4)} key={key}>
                            <ReportButton name={obj.Name} special={obj.Specials[today]} location={this.props.location} delete={this.deleteSpecial} key={key} id={key} />
                            <SpecialCell key={key} name={obj.Name} special={obj.Specials[today]} />
                        </SwipeRow>
                    );
                }
            }
        }
        return specials;
    }

    render() {
        return (
            <ScrollView refreshControl={
                <RefreshControl tintColor='#f7ce3e' refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
            }>
                {this.renderSpecials()}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    swipeRow: {
        padding: 0,
        backgroundColor: '#f4f4f4',
        borderColor: '#dddddd',
        borderBottomWidth: 1,
    }
})