import React from 'react';
import {SafeAreaView, ActivityIndicator, Text, StyleSheet, Dimensions} from 'react-native';

export default class LoadingScreen extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        return(
            <SafeAreaView style={styles.container}>
                <Text style={styles.text}>
                    Loading...
                </Text>
                <ActivityIndicator size="large" color="#f7ce3e" />
            </SafeAreaView>
        );
    }
}

const dimensions = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        display:'flex',
        flexDirection:'column',
        justifyContent:'space-around',
        padding:10,
        margin:'auto',
        height:dimensions.height,
        width:dimensions.width,
        backgroundColor:'#1a2930',
        justifyContent:'center',
        textAlign: 'center',
    },
    text: {
        color: '#eeeeee',
        textAlign: 'center',
        padding:10
    }
});